# P2Element

![](https://bytebucket.org/jthsimopt/p2element/raw/a5752c1b0525e9b560e57ac246c7beddcfac565e/P2Element.png)

    %P2 element class 
    %   O = P2(XC)
    %   O = P2(XC,parameters)
    %   
    %   parameters:
    %   'gorder'   - Gaussorder. Default is 2 (Full).
    %
    %   3
    %   | \
    %   6   5
    %   |     \
    %   1--4---2
    %   Domain is [0,1]

	% XC is a 6-by-d coordinate matrix where d is the dimension, either 2 or 3.
	%
	% 
	% Demo:
	X = [0,0,0; 1,0,0; 0.7,1,0;];
	E = [1,2; 2,3; 3,1];
	XC = [X;(X(E(:,2),:)+X(E(:,1),:))/2];
	scale = 0.1;
	Xr = (rand(3,3)*2-1)*scale;
	XC(4:end,:) = XC(4:end,:)+Xr;
	
	iele = P2Element(XC)
	baseFcn = iele.BaseFunction
	h = iele.VizP2Ele('Refinements',10, 'Light', true, 'FaceColor','r', 'InnerEdgeAlpha',0.4)
	axis equal tight
	
	OUTPUT:
	iele = 

  	P2Element with properties:

               P: [6×3 double]
      GaussOrder: 2
            Area: 0.5841
    BaseFunction: [1×1 struct]
     GaussPoints: [3×3 double]
              nP: 3
	
	baseFcn = 

  	struct with fields:

       fi: [3×6 double]
    dfidx: [3×6 double]
    dfidy: [3×6 double]
    dfidz: [3×6 double]
     detJ: [3×1 double]
     area: [3×1 double]
        n: [3×3 double]
        J: [3×3×3 double]