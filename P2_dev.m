clear
clc
close all

X = [0,0,0;
    1,0,0;
    0.7,1,0;];
E = [1,2;
     2,3;
     3,1];
XC = [X;(X(E(:,2),:)+X(E(:,1),:))/2];

scale = 0.1;
Xr = (rand(3,3)*2-1)*scale;
XC(4:end,:) = XC(4:end,:)+Xr;

k = [1,4,2,5,3,6,1];
xfigure;
% plot3(XC(k,1), XC(k,2), XC(k,3), 'k-o'); hold on; axis equal tight; view(3)
% text(XC(:,1), XC(:,2), cellstr( num2str([1:8]') ), 'BackgroundColor','w')

iele = P2Element(XC)
h = iele.VizP2Ele('Refinements',10, 'Light', true, 'FaceColor','r', 'InnerEdgeAlpha',0.4)
axis equal tight

