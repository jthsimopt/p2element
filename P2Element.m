classdef P2Element
    %P2 element class 
    %   O = P2(XC)
    %   O = P2(XC,parameters)
    %   
    %   parameters:
    %   'gorder'   - Gaussorder. Default is 2 (Full).
    %
    %   3
    %   | \
    %   6   5
    %   |     \
    %   1--4---2
    %   Domain is [0,1]
    
    properties
        P
        GaussOrder
        Area
        BaseFunction
        GaussPoints
        nP
    end
    
    methods
        function O = P2Element(XC, varargin)
            p = inputParser;
            p.addParameter('gorder',2)
            p.parse(varargin{:});
            V = p.Results;
            gorder = V.gorder;
            
            %% Gauss Points
            [gcx,gcy,gv]=trigauc([0,1,0],[0,0,1],gorder);
            GP = [gcx(:), gcy(:)];
            GW = gv(:);
            nGP = size(GP,1);
            
            %% Parametrix Map
            [fi, dfidr, dfids] = ParametricMap(GP);
            
            %% Differentiation
            dim = size(XC,2);
            if dim == 2
                %% 2D
                xc = XC(:,1); yc = XC(:,2);
                dxdr = dfidr*xc;
                dydr = dfidr*yc;
                
                dxds = dfids*xc;
                dyds = dfids*yc;
                
                dXdr = [dxdr, dydr];
                dXds = [dxds, dyds];
                
                J = zeros(2,2,nGP);
                detJ = zeros(nGP,1);
                area = zeros(nGP,1);
                dfidx = zeros(nGP,6);
                dfidy = dfidx;
                rsArea = 1/2;
                for i = 1:nGP
                    Ji = [dXdr(i,:);
                          dXds(i,:);];
                    J(:,:,i) = Ji;
                    
                    dfidri = dfidr(i,:);
                    dfidsi = dfids(i,:);
                    dfidxyi = Ji\[dfidri;
                                  dfidsi;];
                    dfidx(i,:) = dfidxyi(1,:);
                    dfidy(i,:) = dfidxyi(2,:);
                    
                    detJ(i) = det(Ji);
                    area(i) = abs(detJ(i))*rsArea;
                end
                baseFcn.fi = fi;
                baseFcn.dfidx = dfidx;
                baseFcn.dfidy = dfidy;
                baseFcn.detJ = detJ;
                baseFcn.area = area;
                baseFcn.J = J;
                
                GPx = [fi*xc,fi*yc];
                
            elseif dim == 3
                %% 3D
                xc = XC(:,1); yc = XC(:,2); zc = XC(:,3);
                dxdr = dfidr*xc;
                dydr = dfidr*yc;
                dzdr = dfidr*zc;
                
                dxds = dfids*xc;
                dyds = dfids*yc;
                dzds = dfids*zc;
                
                dXdr = [dxdr, dydr, dzdr];
                dXds = [dxds, dyds, dzds];
                n1 = cross(dXdr,dXds,2);
                n = n1./(n1(:,1).^2+n1(:,2).^2+n1(:,3).^2).^0.5;
                
                J = zeros(3,3,nGP);
                detJ = zeros(nGP,1);
                area = zeros(nGP,1);
                dfidx = zeros(nGP,6);
                dfidy = dfidx;
                dfidz = dfidx;
                rsArea = 1/2;
                for i = 1:nGP
                    Ji = [dXdr(i,:);
                          dXds(i,:);
                          n(i,:)];
                    J(:,:,i) = Ji;
                    
                    dfidri = dfidr(i,:);
                    dfidsi = dfids(i,:);
                    dfidti = zeros(1,6);
                    dfidxyzi = Ji\[dfidri;
                                  dfidsi;
                                  dfidti];
                    dfidx(i,:) = dfidxyzi(1,:);
                    dfidy(i,:) = dfidxyzi(2,:);
                    dfidz(i,:) = dfidxyzi(3,:);
                    
                    detJ(i) = det(Ji);
                    area(i) = abs(detJ(i))*rsArea;
                end
                
                baseFcn.fi = fi;
                baseFcn.dfidx = dfidx;
                baseFcn.dfidy = dfidy;
                baseFcn.dfidz = dfidz;
                baseFcn.detJ = detJ;
                baseFcn.area = area;
                baseFcn.n = n;
                baseFcn.J = J;
                
                GPx = [fi*xc,fi*yc,fi*zc];
            end
            
            %%
            O.P = XC;
            O.GaussOrder = gorder;
            O.BaseFunction = baseFcn;
            O.Area = sum(area.*GW);
            O.GaussPoints = GPx;
            O.nP = size(GPx,1);
            
           
        end
        
        function h = VizP2Ele(O,varargin)
            % h = VizP2Ele()
            % h = VizP2Ele(Refinements)
            % h = VizP2Ele(parameters)
            %
            % parameters:
            % Refinements      - Number of refinements, default: 0. Set to -1
            % to display as triangulated.
            % 'FaceColor'      - Default: 'w'
            % 'FaceLighting'   - Default: 'gouraud'
            % 'EdgeColor'      - Default: 'k'
            % 'InnerEdgeColor' - Default: [1,1,1]*0.4
            % 'InnerEdgeAlpha' - Default: 1
            % 'EdgeLineWidth'  - Default: 1.5
            % 'Light'          - Default: false
            
            %% Input Parsin
            p = inputParser;
            addOptional(p,'Refinements',0);
            addParameter(p,'FaceColor','w');
            addParameter(p,'FaceLighting','gouraud');
            addParameter(p,'EdgeColor','k');
            addParameter(p,'InnerEdgeColor',[1,1,1]*0.4);
            addParameter(p,'InnerEdgeAlpha',1);
            addParameter(p,'EdgeLineWidth',1.5);
            addParameter(p,'Light',false);
            parse(p,varargin{:});
            PR = p.Results;
            
            %% Parameters
            P = O.P;
            tri = [1,2,3,4,5,6];
            np = PR.Refinements+2;
            nP2ele=1;
            nTriEle = nP2ele*np^2;
            if nTriEle>30000
                warning('PLOTP2:manyelements',['Number of generates triangular elements is too darn high (',num2str(nTriEle),')!'])
            end
            
            %% Refine mesh
            if np > 1
                dx=1/np;
                [X,Y] = meshgrid(0:dx:1,0:dx:1);
                x=X(:);y=Y(:);
                ind=find(y>eps+1-x);
                x(ind)=[];y(ind)=[];
                
                tt = subTriang(np,x);
                
                T = zeros(nP2ele*np^2,3);
                X = zeros(nP2ele*length(x)^2,3);
                kk = zeros(nP2ele,3*(np+1)-2);
                
                for iel=1:nP2ele
                    iv6=tri(iel,:);
                    %         iv3=tri(iel,1:3);
                    xc=P(iv6,1);yc=P(iv6,2);zc=P(iv6,3);
                    
                    fiP2=[1-3*x+2*x.^2-3*y+4*x.*y+2*y.^2,x.*(-1+2*x),y.*(-1+2*y),-4*x.*(-1+x+y),4*x.*y,-4*y.*(-1+x+y)];
                    %         fiP1=[1-x-y,x,y];
                    
                    xn=fiP2*xc;
                    yn=fiP2*yc;
                    zn=fiP2*zc;
                    
                    % Insert coordinates from iel into X, that will contain all coordinates
                    lx=length(xn);
                    if iel==1
                        X(iel:lx,:)=[xn,yn,zn];
                    else
                        X((iel-1)*lx+1:iel*lx,:)=[xn,yn,zn];
                    end
                    
                    %         tt = subTriang(np,x);
                    
                    % Global triangle matrix is assembled
                    lt=size(tt,1);
                    if iel==1
                        T(iel:lt,:)=tt;
                        tt0=tt;
                        tt1=tt0;
                    else
                        maxtt=max(tt(:));
                        tt1=tt1+maxtt;
                        lt=size(tt1,1);
                        T((iel-1)*lt+1:iel*lt,:)=tt1;
                    end
                    
                    % Global element edge vector is assembled
                    % this is needed to draw the P2 element edges
                    indx=find(x == min(x));
                    indy=find(y == min(y));
                    ns=np+1;
                    ns3=zeros(1,ns);
                    ns3(1,1)=ns;
                    ii=2;
                    for i1=ns-1:-1:1
                        ns3(1,ii)=ns3(1,ii-1)+i1;
                        ii=ii+1;
                    end
                    k = [indx;ns3(2:end)';indy(end-1:-1:1)];
                    
                    k1 = k'+max(kk(:));
                    kk(iel,:) = k1;
                    
                end
            else
                T = tri(:,1:3);
                %         T = [tri(:,[1,2,3]); tri(:,[1,2,4]); tri(:,[1,3,4]); tri(:,[2,4,3])];
                X = P;
            end
            
            %% Draw Graphics
            h.patch =  patch('faces',T,'vertices',X,'FaceColor', PR.FaceColor);
            hold on
            set(h.patch,'FaceLighting', PR.FaceLighting)
            if PR.Light
                hca = gca;
                lightexist = 0;
                for i = 1:length(hca.Children)
                    if isa(hca.Children(i), 'matlab.graphics.primitive.Light')
                        lightexist = 1;
                    end
                end
                if ~lightexist
                    h.light = light;
                end
            end            
            if np > 1
                h.patch.EdgeColor = PR.InnerEdgeColor;
                h.edge=trimesh(kk,X(:,1),X(:,2),X(:,3));
                set(h.edge,'FaceColor','none')
                set(h.edge,'EdgeColor', PR.EdgeColor)
                set(h.edge,'EdgeLighting','none','LineWidth', PR.EdgeLineWidth)
                h.patch.EdgeAlpha = PR.InnerEdgeAlpha;
            else
                h.patch.EdgeColor = PR.EdgeColor;
                h.patch.LineWidth = PR.EdgeLineWidth;
            end
            
            hold off
            
        end
    end
    
end

function [fi, dfidr, dfids] = ParametricMap(rs)
    r = rs(:,1); s = rs(:,2);
    
    fi = [(1-r-s).*(1-2*r-2*s),...
          r.*(2*r-1),...
          s.*(2*s-1),...
          4*r.*(1-r-s),...
          4*r.*s,...
          4*s.*(1-r-s)];
      
    dfidr = [4*s+4*r-3,...
             4*r-1,...
             0*s,...
             -4*(s+2*r-1),...
             4*s,...
             -4*s];
         
    dfids = [4*s+4*r-3,...
             0*r,...
             4*s-1,...
             -4*r,...
             4*r,...
             -4*(2*s+r-1)];
end

function [gcx,gcy,gv]=trigauc(xc,yc,poldegree)
% function [gc,gv]=trigauc(xc,yc,poldegree)
%
%     calculate coordinates and weight for Gauss quadrature
%      in the triangle with vertices in xc(*),yc(*)
%
%     poldegree: degree of polynomial to be integrated exactly (1,2,3,4,5,7,8 or 11)
%
switch(poldegree)
    case 1
        gv=1;
        gcx=(xc(1)+xc(2)+xc(3))/3;
        gcy=(yc(1)+yc(2)+yc(3))/3;
    case 2
        gv=[1/3,1/3,1/3];
        gcx=[(xc(1)+xc(2))/2,(xc(2)+xc(3))/2,(xc(3)+xc(1))/2];
        gcy=[(yc(1)+yc(2))/2,(yc(2)+yc(3))/2,(yc(3)+yc(1))/2];
    case 3
        gv=[3,3,3,8,8,8,27]/60;
        gcx=[xc(1),xc(2),xc(3),(xc(1)+xc(2))/2,(xc(2)+xc(3))/2, ...
            (xc(3)+xc(1))/2,(xc(1)+xc(2)+xc(3))/3];
        gcy=[yc(1),yc(2),yc(3),(yc(1)+yc(2))/2,(yc(2)+yc(3))/2, ...
            (yc(3)+yc(1))/2,(yc(1)+yc(2)+yc(3))/3];
    case 4
        gcx=zeros(1,6);gcy=gcx;gv=gcx;
        x1=0.8168475729804585;y1=0.09157621350977073;z1=1-x1-y1;  % multiplicity 3
        w1=0.3298552309659655/3;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
        gcx(1:3)=xv;gcy(1:3)=yv;gv(1:3)=vv;
        x1=0.1081030181680702;y1=0.4459484909159649;z2=1-x1-y1;  % multiplicity 3
        w1=0.6701447690340345/3;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
        gcx(4:6)=xv;gcy(4:6)=yv;gv(4:6)=vv;
    case 5
        x1=0.101286507323456;
        x2=0.797426985353087;
        x3=x1;
        x4=0.470142064105115;
        x5=x4;
        x6=0.059715871789770;
        x7=1/3;
        xsi=[x1,x2,x3,x4,x5,x6,x7];eta=[x1,x1,x2,x6,x4,x4,x7];
        w1=0.125939180544827;w4=0.132394152788506;w7=0.225;
        gv=[w1,w1,w1,w4,w4,w4,w7];
        gcx=[xsi*xc(1)+eta*xc(2)+(1-xsi-eta)*xc(3)];
        gcy=[xsi*yc(1)+eta*yc(2)+(1-xsi-eta)*yc(3)];
    case 6
        gcx=zeros(1,12);gcy=gcx;gv=gcx;
        x1=0.5014265096581342;y1=0.2492867451709329;z1=1-x1-y1;  % multiplicity 3
        w1=0.3503588271790222/3;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
        gcx(1:3)=xv;gcy(1:3)=yv;gv(1:3)=vv;
        x2=0.8738219710169965;y2=0.06308901449150177;z2=1-x2-y2;  % multiplicity 3
        w2=0.1525347191106164/3;
        [xv,yv,vv]=multiplicity(x2,y2,w2,xc,yc,3);
        gcx(4:6)=xv;gcy(4:6)=yv;gv(4:6)=vv;
        x3=0.6365024991213939;y3=0.05314504984483216;z3=1-x3-y3;  % multiplicity 6
        w3=0.4971064537103575/6;
        [xv,yv,vv]=multiplicity(x3,y3,w3,xc,yc,6);
        gcx(7:12)=xv;gcy(7:12)=yv;gv(7:12)=vv;
    case 7
        x1=0.065130102902216;
        x2=0.869739794195568;
        x3=x1;
        x4=0.312865496004874;
        x5=0.638444188569810;
        x6=0.048690315425316;
        x7=x5;
        x8=x4;
        x9=x6;
        x10=0.260345966079040;
        x11=0.479308067841920;
        x12=x10;
        x13=1/3;
        xsi=[x1,x2,x3,x4,x5,x6,x7,x8,x9,x10,x11,x12,x13];
        eta=[x1,x1,x2,x6,x4,x5,x6,x5,x4,x10,x10,x11,x13];
        w1=0.053347235608838;w4=0.077113760890257;w10=0.175615257433208;
        w13=-0.149570044467682;
        gv=[w1,w1,w1,w4,w4,w4,w4,w4,w4,w10,w10,w10,w13];
        gcx=[xsi*xc(1)+eta*xc(2)+(1-xsi-eta)*xc(3)];
        gcy=[xsi*yc(1)+eta*yc(2)+(1-xsi-eta)*yc(3)];
    case 8
        gcx=zeros(1,16);gcy=gcx;gv=gcx;
        x1=1/3;y1=1/3;z1=1-x1-y1;  % multiplicity 1
        w1=0.1443156076777862;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,1);
        gcx(1)=xv;gcy(1)=yv;gv(1)=vv;
        
        x1=0.08141482341455413;y1=0.4592925882927229;z1=1-x1-y1;  % multiplicity 3
        w1=0.2852749028018549/3;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
        gcx(2:4)=xv;gcy(2:4)=yv;gv(2:4)=vv;
        
        x1=0.8989055433659379;y1=0.05054722831703103;z1=1-x1-y1;  % multiplicity 3
        w1=0.09737549286959440/3;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
        gcx(5:7)=xv;gcy(5:7)=yv;gv(5:7)=vv;
        
        x1=0.6588613844964797;y1=0.1705693077517601;z1=1-x1-y1;  % multiplicity 3
        w1=0.3096521116041552/3;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
        gcx(8:10)=xv;gcy(8:10)=yv;gv(8:10)=vv;
        
        x1=0.0083947774099572110;y1=0.7284923929554041;z1=1-x1-y1;  % multiplicity 6
        w1=0.1633818850466092/6;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,6);
        gcx(11:16)=xv;gcy(11:16)=yv;gv(11:16)=vv;
    case 11
        gcx=zeros(1,28);gcy=gcx;gv=gcx;
        x1=0.9480217181434233;y1=0.02598914092828833;z1=1-x1-y1;  % multiplicity 3
        w1=0.02623293466120857/3;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
        gcx(1:3)=xv;gcy(1:3)=yv;gv(1:3)=vv;
        
        x1=0.8114249947041546;y1=0.09428750264792270;z1=1-x1-y1;  % multiplicity 3
        w1=0.1142447159818060/3;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
        gcx(4:6)=xv;gcy(4:6)=yv;gv(4:6)=vv;
        
        x1=0.01072644996557060;y1=0.4946367750172147;z1=1-x1-y1;  % multiplicity 3
        w1=0.05656634416839376/3;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
        gcx(7:9)=xv;gcy(7:9)=yv;gv(7:9)=vv;
        
        x1=0.5853132347709715;y1=0.2073433826145142;z1=1-x1-y1;  % multiplicity 3
        w1=0.2164790926342230/3;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
        gcx(10:12)=xv;gcy(10:12)=yv;gv(10:12)=vv;
        
        x1=0.1221843885990187;y1=0.4389078057004907;z1=1-x1-y1;  % multiplicity 3
        w1=0.20798741611661160/3;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,3);
        gcx(13:15)=xv;gcy(13:15)=yv;gv(13:15)=vv;
        
        x1=0;y1=0.858870281282263640;z1=1-x1-y1;  % multiplicity 6
        w1=0.04417430269980344/6;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,6);
        gcx(16:21)=xv;gcy(16:21)=yv;gv(16:21)=vv;
        
        x1=0.04484167758913055;y1=0.6779376548825902;z1=1-x1-y1;  % multiplicity 6
        w1=0.2463378925757316/6;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,6);
        gcx(22:27)=xv;gcy(22:27)=yv;gv(22:27)=vv;
        
        x1=1/3;y1=1/3;z1=1-x1-y1;  % multiplicity 1
        w1=0.08797730116222190;
        [xv,yv,vv]=multiplicity(x1,y1,w1,xc,yc,1);
        gcx(end)=xv;gcy(end)=yv;gv(end)=vv;
        
    otherwise
        disp('Polynomial degrees are 1,2,3,4,5,7,8,11 in trigauc')
        gcx=[];gcy=[];gv=[];
end
end

function [xv,yv,vv]=multiplicity(x,y,v,xc,yc,m)
z=[x,y,1-x-y];
switch(m)
    case 1
        xv=[xc(1)*z(1)+xc(2)*z(2)+xc(3)*z(3)];
        yv=[yc(1)*z(1)+yc(2)*z(2)+yc(3)*z(3)];
        vv=v;
    case 3
        xv=[xc(1)*z(1)+xc(2)*z(2)+xc(3)*z(3),xc(1)*z(2)+xc(2)*z(3)+xc(3)*z(1),...
            xc(1)*z(3)+xc(2)*z(1)+xc(3)*z(2)];
        yv=[yc(1)*z(1)+yc(2)*z(2)+yc(3)*z(3),yc(1)*z(2)+yc(2)*z(3)+yc(3)*z(1),...
            yc(1)*z(3)+yc(2)*z(1)+yc(3)*z(2)];
        vv=v*ones(1,3);
    case 6
        xv=[xc(1)*z(1)+xc(2)*z(2)+xc(3)*z(3),xc(1)*z(1)+xc(2)*z(3)+xc(3)*z(2),...
            xc(1)*z(2)+xc(2)*z(1)+xc(3)*z(3),xc(1)*z(2)+xc(2)*z(3)+xc(3)*z(1),...
            xc(1)*z(3)+xc(2)*z(1)+xc(3)*z(2),xc(1)*z(3)+xc(2)*z(2)+xc(3)*z(1)];
        yv=[yc(1)*z(1)+yc(2)*z(2)+yc(3)*z(3),yc(1)*z(1)+yc(2)*z(3)+yc(3)*z(2),...
            yc(1)*z(2)+yc(2)*z(1)+yc(3)*z(3),yc(1)*z(2)+yc(2)*z(3)+yc(3)*z(1),...
            yc(1)*z(3)+yc(2)*z(1)+yc(3)*z(2),yc(1)*z(3)+yc(2)*z(2)+yc(3)*z(1)];
        vv=v*ones(1,6);
end
end

function tt = subTriang(n,x)
    nT=length(x);
    ns=n+1;


    ns3=zeros(1,ns);
    ns3(1,1)=ns;
    ii=2;
    for i1=ns-1:-1:1
        ns3(1,ii)=ns3(1,ii-1)+i1;
        ii=ii+1;
    end
    % ns3; %Third side T

    emax=-1;
    for i1=1:n
        emax=emax+2;
    end
    % emax; %number T in last sector
    elements=1:2:emax;
    nelem = sum(elements); %number elements
    T1 = 1:nT; %number T

    si=1;
    ei=ns;
    T2=zeros(ns);
    for i1=1:ns
        T2(i1,i1:ns) = T1(si:ei);
        si=ei+1;
        ei=ns3(i1)+ns-i1;
    end

    tri2=zeros(nelem,3);
    i1=1;
    giel=1;
    for iseq=elements
        if iseq==1
            seq1 = T2(:,[1,2]);
            ind1= seq1==0;
            seq1(ind1)=[];
            seq1 = [seq1(3),seq1(2),seq1(1)];
            tri2(1,:)=seq1;
            giel=2;
        end
        if iseq~=1
            seqi = T2(:,[i1,i1+1]);
            seqi1=seqi(:,1);
            seqi2=seqi(:,2);
            ind1= seqi1==0;
            seqi1(ind1)=[];
            c1=length(seqi1);

            ind2= seqi2==0;
            seqi2(ind2)=[];
            c2=length(seqi2);

            seqi=seqi(:);
            ind1= seqi==0;
            seqi(ind1)=[];
            seqi = seqi';
            a = c2-1;
            b = iseq - a;
            for ai=1:a
                tri2(giel,:) = seqi([ai,ai+c1+1,ai+c1]);
                giel=giel+1;
            end
            for bi=1:b
                tri2(giel,:) = seqi([bi,bi+1,bi+c1+1]);
                giel=giel+1;
            end
        end
        i1=i1+1;
    end
    tt=tri2;
end